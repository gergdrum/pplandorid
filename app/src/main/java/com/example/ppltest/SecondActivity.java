package com.example.ppltest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SecondActivity extends AppCompatActivity {
    RadioButton[] radioButton;
    String[] stringArray;
    int randomId;
    int randomIndex;
    double numberOfCorrectAnswers;
    double result;
    boolean isChecked;
    List<Integer> randomArray = new ArrayList<>();
    Random random = new Random();
    MyDBHandler myDBHandler = new MyDBHandler(this);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    int currentQuestion = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        SQLiteDatabase database = SQLiteDatabase.openDatabase(getApplicationContext().getFilesDir().getPath() + "/ppldatabaseTest.db",
                null, SQLiteDatabase.OPEN_READONLY);
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        radioGroup.clearCheck();
        System.out.println("!!!!!!!!!!!");
        System.out.println(database);
        System.out.println(database.getMaximumSize());
        System.out.println(database.getPath());
        System.out.println(database.getVersion());
        initFunction();

    }

    private void initFunction() {
        final Button nextButton = findViewById(R.id.next);
        final Button backButton = findViewById(R.id.back);
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        TextView textView = findViewById(R.id.textView);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextSize(20);
        TextView numbersOfQuestion = findViewById(R.id.numberOfQuestions);
        numberOfCorrectAnswers = 0;
        params.setMargins(0, 10, 10, 10);


        numbersOfQuestion.setText((currentQuestion + "/" + myDBHandler.getAllQuestions().size()));
        backButton.setVisibility(View.INVISIBLE);
        nextButton.setText("Következő");
        int count = radioGroup.getChildCount();
        if (count > 0) {
            for (int i = count - 1; i >= 0; i--) {
                View o = radioGroup.getChildAt(i);
                if (o instanceof RadioButton) {
                    radioGroup.removeViewAt(i);
                }
            }
        }

        for (int i = 1; i < myDBHandler.getAllQuestions().size() + 1; i++) {
            randomArray.add(i);
        }

        randomIndex = random.nextInt(randomArray.size());
        randomId = randomArray.get(randomIndex);
        randomArray.remove(randomIndex);
        textView.setText(myDBHandler.getQuestionById(randomId));
        String[] stringArrayTest = new String[myDBHandler.getAllAnswers(randomId).size()];

        for (int i = 0; i < myDBHandler.getAllAnswers(randomId).size(); i++) {
            stringArrayTest[i] = myDBHandler.getAllAnswers(randomId).get(i);
        }

        stringArray = stringArrayTest;
        radioButton = new RadioButton[stringArray.length - 1];


        for (int i = 0; i < stringArray.length - 1; i++) {
            radioButton[i] = new RadioButton(getBaseContext());
            radioButton[i].setText(stringArray[i]);
            radioButton[i].setId(i);
            radioButton[i].setLayoutParams(params);
            radioGroup.addView(radioButton[i]);
        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String text;
                String correctId = stringArray[stringArray.length - 1];
                isChecked = true;
                currentQuestion++;
                if (checkedId == Integer.parseInt(correctId)) {
                    text = getString(R.string.correct);
                    numberOfCorrectAnswers++;
                } else {
                    text = getString(R.string.wrong);
                    radioButton[checkedId].setTextColor(Color.RED);
                }
                for (int i = 0; i < radioButton.length; i++) {
                    radioButton[i].setEnabled(false);
                }
                radioButton[Integer.parseInt(correctId)].setTextColor(Color.GREEN);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

            }
        });


        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView numbersOfQuestion = findViewById(R.id.numberOfQuestions);

                if (isChecked) {
                    numbersOfQuestion.setText(currentQuestion + "/" + myDBHandler.getAllQuestions().size());
                    isChecked = false;
                    final RadioGroup radioGroup = findViewById(R.id.radioGroup);
                    TextView textView = findViewById(R.id.textView);

                    Arrays.fill(radioButton, null);
                    int count = radioGroup.getChildCount();
                    if (count > 0) {
                        for (int i = count - 1; i >= 0; i--) {
                            View o = radioGroup.getChildAt(i);
                            if (o instanceof RadioButton) {
                                radioGroup.removeViewAt(i);
                            }
                        }
                    }


                    if (randomArray.size() > 0) {
                        randomIndex = random.nextInt(randomArray.size());
                        randomId = randomArray.get(randomIndex);
                        randomArray.remove(randomIndex);
                        textView.setText(myDBHandler.getQuestionById(randomId));
                        String[] stringArrayTest = new String[myDBHandler.getAllAnswers(randomId).size()];

                        for (int i = 0; i < myDBHandler.getAllAnswers(randomId).size(); i++) {
                            stringArrayTest[i] = myDBHandler.getAllAnswers(randomId).get(i);
                        }

                        stringArray = stringArrayTest;
                        radioButton = new RadioButton[stringArray.length - 1];


                        for (int i = 0; i < stringArray.length - 1; i++) {
                            radioButton[i] = new RadioButton(getBaseContext());
                            System.out.println(this);
                            radioButton[i].setText(stringArray[i]);
                            radioButton[i].setId(i);
                            radioButton[i].setLayoutParams(params);
                            radioGroup.addView(radioButton[i]);
                        }
                    } else {
                        result = (numberOfCorrectAnswers / myDBHandler.getAllQuestions().size()) * 100;
                        textView.setText("Eredmény: " + (int) result + " %");
                        nextButton.setText("Újra");
                        backButton.setVisibility(View.VISIBLE);

                        nextButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                initFunction();

                            }
                        });
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Válasszon egy opciót", Toast.LENGTH_SHORT).show();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}

