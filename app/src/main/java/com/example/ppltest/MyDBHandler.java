package com.example.ppltest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class MyDBHandler extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "ppldatabaseTest.db";
    public static final String AERODYNAMICS_TABLE_NAME = "aerodynamics";
    public static final String AERODYNAMICS_COLUMN_ID = "id";
    public static final String AERODYNAMICS_COLUMN_QUESTION = "question";
    public static final String AERODYNAMICS_COLUMN_ANSWER = "answer";
    private HashMap hp;

    public MyDBHandler(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub


        db.execSQL();

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        /*db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);*/
    }

    private void openDB(){

    }

    public boolean insertElement(String question, String answer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("question", question);
        contentValues.put("answer", answer);

        db.insert("aerodynamics", null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from aerodynamics where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, AERODYNAMICS_TABLE_NAME);
        return numRows;
    }

    public boolean updateContact(Integer id, String question, String answer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("question", question);
        contentValues.put("answer", answer);
        db.update("aerodynamics", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Integer deleteContact(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("aerodynamics",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public Cursor getAll() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from aerodynamics", null);
        return res;
    }

    public ArrayList<String> getAllAnswers(Integer id) {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from answers where questionId=" + id + "", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex(AERODYNAMICS_COLUMN_ANSWER)));
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<String> getAllQuestions() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from aerodynamics", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex(AERODYNAMICS_COLUMN_QUESTION)));
            res.moveToNext();
        }
        return array_list;
    }

    public String getQuestionById(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from aerodynamics where id=" + id + "", null);
        res.moveToFirst();

        return res.getString(res.getColumnIndex("question"));
    }
}