package com.example.ppltest;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button aerodynamicButton = findViewById(R.id.aerodynamic);
        Button meteorlogyButton = findViewById(R.id.meteorology);
        Button mechanicalStructure = findViewById(R.id.mechanicalStructure);
        Button flightRules = findViewById(R.id.flightRules);
        Button engine = findViewById(R.id.engine);
        Button instruments = findViewById(R.id.instruments);
        Button law = findViewById(R.id.law);

        MyDBHandler myDBHandler = new MyDBHandler(this);

        aerodynamicButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(MainActivity.this, SecondActivity.class));
                    }
                }

        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_aerodynamic) {
            startActivity(new Intent(MainActivity.this, SecondActivity.class));
            return true;
        } else if (id == R.id.action_meterology) {
            startActivity(new Intent(MainActivity.this, SecondActivity.class));
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

}
